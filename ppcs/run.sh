#!/bin/bash

dir=${1:-"."}
echo $dir
if [ ! -d "$dir" ]; then
	>&2 echo directory $dir does not exist
	exit 1
fi

python_http_server=${PYTHON_HTTP:-SimpleHTTPServer}
`python -c "import $python_http_server"` 2> /dev/null
if [ $? -gt 0 ]; then
	>&2 echo was told to use python module $python_http_server as http server, but it is not installed
	exit 1
fi

filelist=$2
if [ -z $filelist ]; then
	filelist="ppcs.dat"
fi
if [ ! -f $filelist ]; then
	if [ ! -z $2 ]; then
		>&2 echo filter file $filelist does not exist
		exit 1	
	fi
	echo "new filter file ppcs.dat created"
	echo "will only match 'requirements.txt'"
	echo requirements.txt > ppcs.dat
fi

package_list_raw=`mktemp`
package_list=`mktemp`
package_dir="./packages"
mkdir -p $package_dir
for r in `cat $filelist`; do
	echo "looking for files matching $r in $dir"
	# all matching files without dotdir in path
	for f in `find $dir -name $r ! -path "*/.[^.]*"`; do
		echo "\tfound $f"
		cat $f >> $package_list_raw
	done
done

if [ `stat --printf %s $package_list_raw` == '0' ]; then
	>&2 echo no package requirements found, exiting
	exit 1
fi
cat $package_list_raw | uniq | sort > $package_list


for p in `cat $package_list`; do
	echo downloading $p
	pip download -d $package_dir $p
done

ls $package_dir

pushd $package_dir
bash ../pep503.sh .
python -m $python_http_server
popd


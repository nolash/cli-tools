#!/bin/bash

ZON=(Europe/London Europe/Berlin Asia/Kolkata)
for z in ${ZON[@]}; do
	echo -en "$z:\t"
	TZ=$z date
done

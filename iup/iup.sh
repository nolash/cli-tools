#!/bin/bash

f="/run/user/$UID/probe_up"
rm -f $f

up() {
	#ip route | awk '{ if (/^default/) c=$5; if (c ~ /^tun/ ) { o="VPN"; c=0; } if ( c ~ /^wlp/ ) { o="CLEAR"; c=0; } } END { print o }'
	#ip route | awk 'BEGIN { o="CLEAR";} { if ( /^0.0.0.0/ ) { if ($5 ~ /^tun/) { o="VPN"; c=""; } } } END { print o; }' >&3
	#truncate -s0 $f
	#exec 3<>$f
	#r=`ip route | awk 'BEGIN { o="CLEAR";} { if ( $2 ~ dev && $3 ~ /^tun/ ) o="VPN" } END { print o; }'`
       	#echo -e "\033[32m$r\033[39m" >&3
	#exec 3>&-
	#exit 0
	if [ ! -f $f ]; then
		logger -p info "iup up"
	fi
	touch $f
}
down() {
	#truncate -s0 $f
	#exec 3<>$f
	#echo "DOWN" >&3
	#exec 3>&-
	#exit 0
	if [ -f $f ]; then
		logger -p info "iup down"
	fi
	rm -f $f
}

argh() {
	logger -p info "iup die"
	rm -f $f
	exit 0
}

IUP_DELAY=${IUP_DELAY:-$1}
if [ -z $IUP_DELAY ]; then
	IUP_DELAY=5
fi
logger -p debug iup started with delay $IUP_DELAY

hosts=()
while read h; do
	hosts+=($h)
done < $HOME/.config/iup 2> /dev/null
if [ ${#hosts[@]} -eq "0" ]; then
	# use cnn, google as default
	hosts=(8.8.8.8 151.101.193.67)
	logger -p warn missing hosts input file, using default: ${hosts[@]}
fi

trap argh SIGINT
trap argh SIGTERM
trap argh SIGQUIT

while true; do
	isup=""
	for h in ${hosts[@]}; do
		ping -c1 $h -w1 -q &> /dev/null
		if [ $? -eq '0' ]; then
			up
			isup=1
			#logger -p debug "ping success $h"
			break
		fi
	done
	if [ -z $isup ]; then
		down
	fi
	sleep $IUP_DELAY
done

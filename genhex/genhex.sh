#!/bin/bash

if [ -z $1 ]; then
	cat <<EOF
Usage: genhex <number of bytes>
EOF
	exit 1
fi

dd if=/dev/urandom bs=$1 count=1 2> /dev/null | hexdump -n $1 -e '1/1 "%02X"'

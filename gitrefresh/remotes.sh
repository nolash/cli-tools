# WIP

git_at_to_https() {
	url=''
	if [ ${1:0:4} == 'git@' ]; then
		url_right=`echo ${1:4} | sed -e 's/:/\//'`
		url="https://${url_right}"
	fi
	echo $url
}

remotes() {
	url=`git remote -v | awk '$3 ~ /fetch/ && $1 ~ /^origin$/ { print $2; }'`
	if [ -z $url ]; then
		>&2 echo "no origin, please fix that!"
		>&2 git remote -v
		return 1
	fi

	if [ ${url:0:6} == 'ssh://' ]; then
		>&2 echo "origin is ssh url $r"
	# github, gitlab
	elif [ ${url:0:4} == 'git@' ]; then
		>&2 echo "origin is git@ login $r"
	else
			fi
	
	if [ -z $url ]; then
		>&2 echo "no urls left to try, trying to translate origin $origin"
		dirtyname=`sha256sum <(echo -n $origin) | sha1sum | awk '{print $1;}'`
		if [ ${r:0:4} == 'git@' ]; then
			url=$(git_at_to_https $r)
		fi
		git remote -v add $dirtyname $url
	fi
	for u in $url; do
		git remote update $u
	done
	if [ ! -z $dirtyname ]; then
		git remote -v rm $dirtyname
	fi
}

remotes

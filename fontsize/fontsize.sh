#!/bin/bash

small='7x13'
medium='9x15'
large='10x20'
default=$small

list() {
	FP=`xset q | grep ^Font -A1 | tail -n1 | sed -e "s@^[[:space:]]*\(/[^,]*\),.*@\1@"`
	SZ=`find $FP -regextype grep -regex ".*/[0-9][0-9]\?x[0-9][0-9]\?\..*" | sort  | sed -e "s/^.*\/\([0-9x]*\)\..*/\1/"`
	for sz in $SZ; do
		>&2 echo $sz
	done
	exit 1
}

case "$1" in

'r' )
	c=$default
	;;

's' )
	c=$small
	;;

'm' )
	c=$medium
	;;

'l' )
	c=$large
	;;
* )
	c=$1
esac

[[ $c =~ [0-9][0-9]?x[0-9][0-9]? ]] && echo -ne "\033]710;$c\033\\" && exit 0 || list

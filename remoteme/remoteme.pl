#!/usr/bin/env perl

use Errno qw/ENODATA/;
use Getopt::Long qw/GetOptions/;
use STUN::Client;
use 5.010; # used in example in https://perlmaven.com/how-to-process-command-line-arguments-in-perl
use Data::Dumper;
use File::BaseDir qw/config_files/;

use constant DEFAULT_SERVER_LIST_PATH => '/usr/share/stun/servers.txt';

my $_remotestring;
my $nonewline;
my $configfile;
GetOptions(
	'd' => \$debug,
	'n' => \$nonewline,
	'host=s' => \$_remotestring,
	'c=s' => \$configfile,
);

if ($configfile eq '') {
	$configfile = config_files('remoteme/servers.txt');
}

if ($configfile eq '') {
	if (! -f DEFAULT_SERVER_LIST_PATH) {
		if ($_remotestring eq '') {
			print STDERR qq{server list missing\n};
			exit 1;
		}	
	}
	$configfile = DEFAULT_SERVER_LIST_PATH;
}

my @servers;
if ($_remotestring) {
	#$remotestring = $_remotestring;
	push @servers, $_remotestring;
} else {
	if ($debug) {
		print STDERR "using config file " . $configfile . "\n";
	}
	my $f;
	open($f, "<", $configfile);
	foreach my $l (<$f>) {
		push @servers, $l;
	}
	close($f);
	
}

my $result;
foreach my $remotestring (@servers) {
	my $host = $remotestring;
	my $port = 3478;
	if ($remotestring =~ ':') {
		($host, $port) = split(/:/, $remotestring);
	}

	if ($debug) {
		print STDERR "using host " . $remotestring;
	}

	$c = STUN::Client->new;

	eval {
		$c->stun_server($host);
	} or do {
		exit $!;
	};
	eval {
		($old, $new) = $c->get or die "cannot get $!";
	} and do {
		undef $old;
		$result = $new->{'attributes'}{'XOR-MAPPED-ADDRESS'}{'address'};
		if (!defined $new || $result eq '') {
			exit ENODATA;
		}
		print $result;
		unless ($nonewline) {
			print "\n";
		}
		#print Dumper($new);
		exit 0;
	};

	undef $old;
}

__END__

=head1 NAME

rmme - Print current public internet address

=head1 SYNOPSIS

rmme [-nh]

=head1 DESCRIPTION

Queries a STUN server for the local host's public internet address and returns the result.

If no address is reported ENODATA will be returned.

=head1 OPTIONS

=over 2

=item -n	Omit newline at end of result

=item -h	Stun server hostname in format <host|ip>[:port]

=back

=head1 AUTHOR

Louis Holbrook <dev@holbrook.no> (https://holbrook.no)

=head1 COPYRIGHT

Licenced under GPLv3

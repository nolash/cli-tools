# Python package cache server

Downloads dependencies for requirements files and serves them locally

## USAGE

```
run.sh <directory> [listfile]
```

Will recursively scan `directory` for files with file name listed in `listfile`.

If `listfile` is not given, the file `ppcs.dat` is used.

If `ppcs.dat` does not exist, it will be created with `requirements.txt` as the only filename.

`listfile` must have one filename per line.

Once directory scan and downloads have been completed, it will serve the files on localhost:8000

To install files using this repo, use `-i http://localhost:8000` as argument to `pip install`

## DEPENDENCIES

`SimpleHTTPServer` python module. If you want to use `RangeHTTPServer` instead set the `PYTHON_HTTP` environment variable to `RangeHTTPServer`
